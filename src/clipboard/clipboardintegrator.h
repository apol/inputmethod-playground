#include "quickinputplugin.h"

#include <QStringListModel>

class InputMethodContext;

class ClipboardIntegrator : public QuickInputPlugin
{
    Q_OBJECT
    Q_PROPERTY(QStringListModel* entryModel READ entryModel CONSTANT)
    Q_PROPERTY(int currentIndex MEMBER m_currentIndex NOTIFY currentIndexChanged)
public:
    ClipboardIntegrator(InputMethod *inputMethod);
    ~ClipboardIntegrator();
    QStringListModel* entryModel();

    void addEntry(const QString &entry);
    void keyPressed(QKeyEvent *keyEvent) override;

Q_SIGNALS:
    void currentIndexChanged();
private:
    void onShortcut();
    void onCommit();
    QString currentValue();

    bool m_active = false;
    int m_currentIndex = 0;
    QStringListModel *m_entryModel;

};

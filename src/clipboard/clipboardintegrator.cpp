#include "clipboardintegrator.h"


#include <KSystemClipboard>
#include <QMimeData>

ClipboardIntegrator::ClipboardIntegrator(InputMethod *inputMethod)
    : QuickInputPlugin(inputMethod)
    , m_entryModel(new QStringListModel(this))
{
    setSource(QUrl("qrc:/clipboard/main.qml"));
    setGrabbing(true);

    auto clip = KSystemClipboard::instance();
    connect(clip, &KSystemClipboard::changed, this, [this](QClipboard::Mode mode) {
        if (mode != QClipboard::Clipboard) {
            return;
        }
        const QMimeData *mimeData = KSystemClipboard::instance()->mimeData(mode);
        if (!mimeData) {
            return;
        }
        // Before shipping as a real product, this would want to dedeupe, filter lists
        // but we would want to not have two processes doing klipper's job anyway
        if (mimeData->hasText()) {
            addEntry(mimeData->text());
        }
    });
}

ClipboardIntegrator::~ClipboardIntegrator() {}

QStringListModel *ClipboardIntegrator::entryModel()
{
    return m_entryModel;
}

void ClipboardIntegrator::addEntry(const QString &entry)
{
    const int row = 0; //prepend
    m_entryModel->insertRow(row);
    m_entryModel->setData(m_entryModel->index(row, 0), entry);
}

void ClipboardIntegrator::keyPressed(QKeyEvent *keyEvent)
{
    if (keyEvent->key() == Qt::Key_V && keyEvent->modifiers().testFlags({Qt::ControlModifier, Qt::ShiftModifier})) {
        onShortcut();
        keyEvent->setAccepted(true);
    } else {
        if (m_active) {
            onCommit();
        }
    }
    QuickInputPlugin::keyPressed(keyEvent);
}

void ClipboardIntegrator::onShortcut() {

    if (m_active) {
        m_currentIndex++;
        if (m_currentIndex >= m_entryModel->rowCount()) {
            m_currentIndex = 0;
        }
        Q_EMIT currentIndexChanged();
    } else {
        m_active = true;
        showWindow();
    }

    const QString clip = currentValue();
    const QString clipMessage = QStringLiteral("%1 (%2/%3)").arg(clip, QString::number(m_currentIndex + 1), QString::number(m_entryModel->rowCount()));

    static int goodStyle = 3;
    setPreEditStyle(0, clip.size(), goodStyle);
    setPreEditStyle(clip.size(), clipMessage.size() - clip.size(), 5);

    setPreEditCursor(clipMessage.size());
    setPreEditString(clipMessage);
}

void ClipboardIntegrator::onCommit() {
    commit(currentValue());
    m_active = false;
    hideWindow();
    m_currentIndex = 0;
    Q_EMIT currentIndexChanged();
}

QString ClipboardIntegrator::currentValue()
{
    if (m_currentIndex > m_entryModel->rowCount()) {
        return QString();
    }
    return m_entryModel->stringList().at(m_currentIndex);
}

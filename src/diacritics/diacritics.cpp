#include "diacritics.h"
#include "plasmakeydata.h"

#include <KSystemClipboard>
#include <QMimeData>

Diacritics::Diacritics(InputMethod *inputMethod)
    : QuickInputPlugin(inputMethod)
    , m_charactersModel(new QStringListModel(this))
{
    setSource(QUrl("qrc:/diacritics/main.qml"));
    setGrabbing(true);

    m_isHeldTimer.setInterval(1000);
    m_isHeldTimer.setSingleShot(true);
    connect(&m_isHeldTimer, &QTimer::timeout, this, &Diacritics::onHeldTimeout);

    connect(this, &InputPlugin::contextChanged, &m_isHeldTimer, [this]() {
        m_isHeldTimer.stop();
        m_charactersModel->setStringList({});
    });

    connect(m_charactersModel, &QAbstractItemModel::rowsInserted, this, &Diacritics::activeChanged);
    connect(m_charactersModel, &QAbstractItemModel::rowsRemoved, this, &Diacritics::activeChanged);
    connect(m_charactersModel, &QAbstractItemModel::modelReset, this, &Diacritics::activeChanged);
}

Diacritics::~Diacritics() {}

void Diacritics::keyPressed(QKeyEvent *keyEvent)
{
    if (KeyData::KeyMappings.contains(keyEvent->text()) && !keyEvent->modifiers()) {
        m_pressedKey = keyEvent->text();
        m_isHeldTimer.start();

        //a made up hack to tell the backend to send a key release
        //maybe subclass KeyEvent to have some new options?
        keyEvent->setTimestamp(-1);
        //maybe this should be an "if ascii letter" for consistency

        // or maybe we should block the key completely and do a pre-edit string for the letter
    }
    // give the UI a chance to grab hotkeys
    QuickInputPlugin::keyPressed(keyEvent);

    if (!keyEvent->isAccepted()) {
        m_charactersModel->setStringList({});
    }
}

void Diacritics::commitTranslation(const QString &text)
{
    deleteSurroundingText(-1, 1);
    commit(text);
}

void Diacritics::keyReleased(QKeyEvent *keyEvent)
{
    m_pressedKey = QString();
    m_isHeldTimer.stop();
    QuickInputPlugin::keyPressed(keyEvent);
}

bool Diacritics::isActive()
{
    return m_charactersModel->rowCount() > 0;
}

QStringListModel *Diacritics::charactersModel()
{
    return m_charactersModel;
}

void Diacritics::onHeldTimeout()
{
    m_charactersModel->setStringList(KeyData::KeyMappings.value(m_pressedKey));
}

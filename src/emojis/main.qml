/*
This file is part of the KDE project.

SPDX-FileCopyrightText: 2016 Martin Flöser <mgraesslin@kde.org>
SPDX-FileCopyrightText: 2021 Aleix Pol Gonzalez <aleixpol@kde.org>

SPDX-License-Identifier: GPL-2.0-or-later
*/
import QtQuick
import QtQuick.Layouts
import QtQuick.Window
import QtQuick.Controls as QQC2
import org.kde.kirigami as Kirigami
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.extras as PlasmaExtras

import org.kde.plasma.emoji

ListView {
    id: emojiLayout
    implicitWidth: PlasmaCore.Units.iconSizes.large * Math.min(emojiLayout.count, 5)
    implicitHeight: PlasmaCore.Units.iconSizes.large
    orientation: Qt.Horizontal
    highlightFollowsCurrentItem: true
    highlightRangeMode: ListView.ApplyRange

    highlight: PlasmaExtras.Highlight { }

    EmojiModel {
        id: emoji
    }

    Binding {
        target: InputPlugin
        property: "windowVisible"
        value: InputPlugin.bracketedText.length > 0 && emojiLayout.count
    }

    focus: true

    Keys.onReturnPressed: {
        currentItem.reportEmoji()
    }

    model: SearchModelFilter {
        sourceModel: emoji
        search: InputPlugin.bracketedText
    }

    Connections {
        target: InputPlugin
        function onContextChanged() { currentIndex = 0}
    }

    delegate: MouseArea {
        QQC2.Label {
            font.pointSize: 25
            font.family: 'emoji' // Avoid monochrome fonts like DejaVu Sans
            fontSizeMode: model.display.length > 5 ? Text.Fit : Text.FixedSize
            minimumPointSize: 10
            text: model.display
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            anchors.fill: parent
            anchors.margins: 1
        }

        width: PlasmaCore.Units.iconSizes.large
        height: PlasmaCore.Units.iconSizes.large

        QQC2.ToolTip.delay: Kirigami.Units.toolTipDelay
        QQC2.ToolTip.text: model.toolTip
        QQC2.ToolTip.visible: mouse.containsMouse

        function reportEmoji() {
            InputPlugin.replaceBracket(model.display)
        }

        id: mouse
        hoverEnabled: true
        onClicked: reportEmoji()
    }

}

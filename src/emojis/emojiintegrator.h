/*
    SPDX-FileCopyrightText: 2021 Aleix Pol Gonzalez <aleixpol@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#pragma once

#include <QObject>
#include "quickinputplugin.h"

class EmojiIntegrator : public QuickInputPlugin
{
    Q_OBJECT
    Q_PROPERTY(QString bracketedText READ bracketedText NOTIFY bracketedTextChanged)

public:
    EmojiIntegrator(InputMethod *inputMethod);
    ~EmojiIntegrator();
    QString bracketedText() const
    {
        return m_bracketedText;
    }

    Q_INVOKABLE void replaceBracket(const QString &text);

Q_SIGNALS:
    void bracketedTextChanged(const QString &bracketedText);

private:
    void setBracketedText(const QString &text);
    void refreshBracketedText();

    QString m_bracketedText;
};
